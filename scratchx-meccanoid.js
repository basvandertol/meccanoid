(function(ext) {
    // Cleanup function when the extension is unloaded
    ext._shutdown = function() {};

    // Status reporting code
    // Use this to report missing hardware, plugin or unsupported browser
    ext._getStatus = function() {
        return {status: 2, msg: 'Ready'};
    };

    ext.eye_lights = function(r,g,b) {
        // Code that gets executed when the block is run

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://127.0.0.2:8000/demo_post2.asp", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        msg = "eye_lights" + '\\n' +
              r + '\\n' +
              g + '\\n' +
              b + '\\n' +
              '\\n';

        xhttp.send(msg);
        console.log(msg)

    };

    ext.servo = function(id, position) {
        // Code that gets executed when the block is run

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://127.0.0.2:8000/demo_post2.asp", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        msg = "servo" + '\\n' +
              id + '\\n' +
              position + '\\n' +
              '\\n';

        xhttp.send(msg);
        console.log(msg)

    };

    ext.move = function(left, right) {
        // Code that gets executed when the block is run

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://127.0.0.2:8000/demo_post2.asp", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        msg = "move" + '\\n' +
              left + '\\n' +
              right + '\\n' +
              '\\n';

        xhttp.send(msg);
        console.log(msg)

    };

    // Block and block menu descriptions
    var descriptor = {
        blocks: [
            // Block type, block name, function name
            [' ', 'eye light red%d.value green%d.value blue%d.value', 'eye_lights', 0 ,0 , 0],
            [' ', 'servo %d.value %d', 'servo', 0 ,0],
            [' ', 'set speed %d %d', 'move', 0 ,0]
        ],
        menus: {
            value:[0,1,2,3,4,5,6,7]}
    };

    // Register the extension
    ScratchExtensions.register('Meccanoid', descriptor, ext);
})({});

